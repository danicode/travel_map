<?php

class AuthController extends BaseController {

    public function status() {
        return Response::json(Auth::check());
    }

    public function secrets() {
        if(Auth::check()) {
            return 'You are logged in, here are secrets.';
        } else {
            return 'You aint logged in, no secrets for you.';
        }
    }

    public function post_login()
    {
        $input = Input::all(); 
        
        $language = Input::json('language') != null ? Input::json('language') : 'gb';        
        App::setLocale($language);

        $validator = Validator::make($input, User::$rulesLogin);         

        if($validator->fails())
        {                 
            $validator->setAttributeNames(User::getAttributeNamesLogin()); 
        
            $messages = $validator->messages();     
            
            $message_concat = implode("\n", $messages->all());         

            $errors = [
                'type'    => 'login_error',
                'message' => $message_concat
            ];
            
            return Response::json(array('flash' => $errors), 400);
        }

        $credentials = [
            User::COLUMN_EMAIL    => Input::json(User::COLUMN_EMAIL),
            User::COLUMN_PASSWORD => Input::json(User::COLUMN_PASSWORD)
        ];

        if(Auth::attempt($credentials))
        {
            return Response::json(Auth::user(), 200);
        } else {
            $errors = [
                'type'    => 'login_error',
                'message' => Lang::get('validation.error_message_login')
            ];
            return Response::json(array('flash' => $errors), 400);
        }
    }

    public function get_logout()
    {
        Auth::logout(); 
        
        $language = Input::json('language') != null ? Input::json('language') : 'gb';        
        App::setLocale($language);
        
        $messages = [
            'type'    => 'logout_success',
            'message' => Lang::get('general.successful_logout_message')
        ];
        return Response::json(array('flash' => $messages), 200);
    }
    
    public function post_signup() {
        $input = Input::all(); 
        
        $language = Input::json('language') != null ? Input::json('language') : 'gb';        
        App::setLocale($language);
        
        $validator = Validator::make($input, User::$rulesSignup); 

        if($validator->fails())
        {                 
            $validator->setAttributeNames(User::getAttributeNamesSignup()); 
        
            $messages = $validator->messages();     
            
            $message_concat = implode("\n", $messages->all());         

            $errors = [
                'type'    => 'signup_error',
                'message' => $message_concat
            ];
            
            return Response::json(array('flash' => $errors), 400);
        }      

        $user = new User;
        $user->email      = Input::get(User::COLUMN_EMAIL);
        $user->password   = Hash::make(Input::get(User::COLUMN_PASSWORD));
        $user->first_name = Input::get(User::COLUMN_FIRST_NAME);
        $user->last_name  = Input::get(User::COLUMN_LAST_NAME);
        $user->save();
        
        // Check if there is a folder with the user's email. If not exist create the folder.
        if(!is_dir(public_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.$user->email)){        
            mkdir(public_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.$user->email, 0755);
        }

        //Change var $message to $content, because the var $message is a keyword.
        $data = array( 
            'name'     => Input::get(User::COLUMN_FIRST_NAME),
            'email'    => Input::get(User::COLUMN_EMAIL),
            'subject'  => Lang::get('general.message_success_register'),
            'content'  => Input::get('message')
        );

        Mail::send('emails.register', $data, function($message)
        {
            $message->from(Lang::get('general.email_from'), Lang::get('general.title'));

            $message->to(Input::get(User::COLUMN_EMAIL))->subject(Lang::get('general.message_success_register'));
        }); 
        
        $messages = [
            'type'    => 'register_success',
            'message' => Lang::get('general.message_success_register')
        ];

        return Response::json(array('flash' => $messages), 200);
    }

}