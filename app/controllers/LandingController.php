<?php

class LandingController extends BaseController {
    
    public function index() { 

        // Change delimiters to avoid conflicts with angularjs.
        
        Blade::setEscapedContentTags('[[', ']]');
        Blade::setContentTags('[[[', ']]]');
        
        return View::make('index');
    }

}