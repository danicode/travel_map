<?php

/**
 * Description of RemindersController
 *
 * @author dani
 */
class RemindersController extends BaseController {
    
   /**
    * Handle a POST request to remind a user of their password.
    *
    * @return Response
    */
    public function post_remind()
    {    
        $language = Input::json('language') != null ? Input::json('language') : 'gb';        
        App::setLocale($language);
        
        switch ($response = Password::remind(Input::only(User::COLUMN_EMAIL)))
        {
            case Password::INVALID_USER:       

                $errors = [
                    'type'    => 'error_reset',
                    'message' => Lang::get('general.error_reset_message')
                ];
            
                return Response::json(array('flash' => $errors), 400);

            case Password::REMINDER_SENT:
      
                $message = [
                    'type'    => 'success_reset',
                    'message' => Lang::get('general.success_reset_message')
                ];
                
                return Response::json(array('flash' => $message), 200);
        }
    }
    
   /**
    * Display the password reset view for the given token.
    *
    * @param  string  $token
    * @return Response
    */
    public function get_reset($token = null)
    {        
        if (is_null($token)) 
        {
            
            $language = Input::json('language') != null ? Input::json('language') : 'gb';        
            App::setLocale($language);
            
            $errors = [
                'type'    => 'error_token',
                'message' => Lang::get('reminders.token')
            ];

            return View::make('index')->with('error', $errors);
        }

        return View::make('index')->with(User::FORM_TOKEN, $token);
    }
    
   /**
    * Handle a POST request to reset a user's password.
    *
    * @return Response
    */
    public function post_reset()
    {
        $credentials = Input::only(
            User::COLUMN_EMAIL, 
            User::COLUMN_PASSWORD, 
            User::FORM_PASSWORD_CONFIRMATION, 
            User::FORM_TOKEN
        );

        $validator = Validator::make($credentials, User::$rulesResetPassword); 

        $validator->setAttributeNames(User::getAttributeNamesResetPassword()); 

        $response = Password::reset($credentials, function($user, $password)
        {
            $user->password = Hash::make($password);
            $user->save();
        });

        switch ($response)
        {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                if($validator->fails())
                {                    
                    $validator->setAttributeNames(User::getAttributeNamesResetPassword()); 
        
                    $messages = $validator->messages();     

                    $message_concat = implode("\n", $messages->all());         

                    $errors = [
                        'type'    => 'error_reset_password',
                        'message' => $message_concat
                    ];

                    return Response::json(array('flash' => $errors), 400);
                    
                }                
                $errors = [
                    'type'    => 'error_reset_password',
                    'message' => Lang::get($response)
                ];

                return Response::json(array('flash' => $errors), 400);

            case Password::PASSWORD_RESET:                
                $message = [
                    'type'    => 'success_reset_password',
                    'message' => Lang::get('general.successful_reset_message')
                ];
                
                return Response::json(array('flash' => $message), 200);
        }
    }
}