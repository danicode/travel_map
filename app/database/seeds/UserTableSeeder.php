<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserTableSeeder
 *
 * @author dani
 */
class UserTableSeeder extends DatabaseSeeder {
    
    public function run() {
        
        $users = [
            [
                'email'      => 'danielanzawa@gmail.com',
                'password'   => Hash::make('dani123546'),
                'first_name' => 'Daniel José',
                'last_name'  => 'Anzawa',
                'image_url'  => ''                
            ]
        ];

        foreach ($users as $user)
        {
            User::create($user);
        }
        
    }
}