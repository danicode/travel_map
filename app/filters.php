<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
    //
});


App::after(function($request, $response)
{
    //
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
    if (Auth::guest()) return Redirect::guest('login');
});

Route::filter('auth_rest', function()
{
    if (!Auth::check()) {
        $language = Input::json('language') != null ? Input::json('language') : 'gb';        
        App::setLocale($language);
        
        $error = [
            'type'    => 'remind_guest_filter',
            'message' => Lang::get('general.auth_filter')
        ];
        return Response::json(array('flash' => $error), 401);
    }
});


Route::filter('auth.basic', function()
{
    return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
    if (Auth::check()) return Redirect::to('/');    
});

Route::filter('guest_rest', function()
{
    if (Auth::check()) {
        $language = Input::json('language') != null ? Input::json('language') : 'gb';        
        App::setLocale($language);
        
        $messages = [
            'type'    => 'remind_guest_filter',
            'message' => Lang::get('reminders.remind_guest_filter')
        ];
        return Response::json(array('flash' => $messages), 400);
    }    
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
    if (Session::token() != Input::get('_token'))
    {
        throw new Illuminate\Session\TokenMismatchException;
    }
});

Route::filter('csrf_json', function() 
{
    if (Session::token() != Input::json('csrf_token')) 
    {        
        $language = Input::json('language') != null ? Input::json('language') : 'gb';
        
        App::setLocale($language);
        
        $errors = [
            'type'    => 'token_mismatch_exception',
            'message' => Lang::get('general.message_token_mismatch_exception')
        ];

        return Response::json(array('flash' => $errors), 500);  
    }
});