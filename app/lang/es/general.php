<?php 

return array(
    
    'title'                            => 'TRAVEL MAP',
    'first_name'                       => 'Nombre',
    'last_name'                        => 'Apellido',
    'email'                            => 'Correo electrónico',
    'password'                         => 'Contraseña',
    'password_confirmation'            => 'Confirmar contraseña',
    'message_token_mismatch_exception' => 'HTTP Error 500. Para su correcto funcionamiento deberá recargar la página, disculpe los inconvenientes.',
    'message_success_register'         => 'Te has registrado correctamente.',
    'successful_logout_message'        => 'Has cerrado sesión correctamente.',
    'auth_filter'                      => 'Por favor, inicie sesión.',
    
   /*
    |------------------|
    | Email register   |
    |------------------|
    */

    'email_register_message_I'   => 'Te has registrado correctamente',
    'email_register_greeting'    => 'Muchas gracias por registrarte en nuestro sitio :)',
    'email_register_message_II'  => 'Esperamos que pronto puedas completar el map con tus viajes.',
    'email_register_message_III' => 'Has click aquí para ir a',
    'email_from'                 => 'travelmap@travelmap.com',
    
   /*
    |------------------------|
    | Form reset password    |
    |------------------------|
    */

    'message_reset_password'   => 'Para restablecer su contraseña, complete este formulario:',
    'successful_reset_message' => 'Se ha restablecido la contraseña exitósamente. Intente ingresar a su cuenta nuevamente por favor.',
    'error_reset_message'      => 'No es posible restablecer la contraseña. Por favor, compruebe los datos.',
    'success_reset_message'    => 'Se ha enviado correctamente los datos. Por favor revise su correo electrónico.',
    'password_reset'           => 'Restablecimiento de contraseña',
    
   /*
    |------------------------|
    | Code HTTP Status       |
    |------------------------|
    */
    
    'lbl_code_http_status_200'             => 'HTTP 200 OK. Respuesta estándar para peticiones correctas.',
    'lbl_code_http_status_400'             => 'HTTP Error 400: La solicitud contiene sintaxis errónea y no debería repetirse.',
    'lbl_code_http_status_401'             => 'HTTP Error 401: Acceso no autorizado.',  
    'lbl_code_http_status_402'             => 'HTTP Error 402: Requiere de pago.',
    'lbl_code_http_status_403'             => 'HTTP Error 403: Recurso prohibido.',
    'lbl_code_http_status_404'             => 'HTTP Error 404: No se encuentra la página solicitada.',
    'lbl_code_http_status_407'             => 'HTTP Error 407: Requiere autenticación del proxy.',
    'lbl_code_http_status_415'             => 'HTTP Error 415: Tipo de medio no soportado.',
    'lbl_code_http_status_500'             => 'HTTP Error 500: Error interno del servidor.',  
    'lbl_code_http_status_501'             => 'HTTP Error 501: Recurso no implementado.',
    'lbl_code_http_status_502'             => 'HTTP Error 502: Gateway incorrecto.',
    'lbl_code_http_status_503'             => 'HTTP Error 503: Servicio no disponible.',           
    'lbl_code_http_status_500_clean_cache' => 'HTTP Error 500. Si ha borrado la caché, por favor, actualice el navegador por favor.',

);