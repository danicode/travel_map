<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password"            => "La contraseña debe tener ocho caracteres y coincidir con la confirmación.",
	"user"                => "No podemos encontrar un usuario con esa dirección de correo electrónico.",
	"token"               => "Esta señal de restablecimiento de contraseña no es válida.",    
        "sent"                => "¡Recuperación de contraseña enviado!",
        "remind_guest_filter" => "Estás autentificado.",

);