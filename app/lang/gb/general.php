<?php 

return array(
    
    'title'                            => 'TRAVEL MAP',
    'first_name'                       => 'First name',
    'last_name'                        => 'Last name',
    'email'                            => 'Email',
    'password'                         => 'Password',
    'password_confirmation'            => 'Confirm password',
    'message_token_mismatch_exception' => 'HTTP Error 500. To function properly you need refresh the page, excuse me for the inconvenience.',
    'message_success_register'         => 'You have successfully signed.',
    'successful_logout_message'        => 'You have successfully logged out.',
    'auth_filter'                      => 'Please log in.',
    
   /*
    |------------------|
    | Email register   |
    |------------------|
    */

    'email_register_message_I'   => 'You have successfully signed',
    'email_register_greeting'    => 'Thank you very much for registering on our website :)',
    'email_register_message_II'  => 'We hope that soon you can complete the map with your travels.',
    'email_register_message_III' => 'Click here for go to',
    'email_from'                => 'travelmap@travelmap.com',
    
   /*
    |------------------------|
    | Form reset password    |
    |------------------------|
    */

    'message_reset_password'   => 'To reset your password, complete this form:',
    'successful_reset_message' => 'Password has been reset successfully. Try logged your account again please.',
    'error_reset_message'      => 'Unable to reset password. Please check the data.',
    'success_reset_message'    => 'You have successfully sent the data. Please check your email.',
    'password_reset'           => 'Password reset',
    
   /*
    |------------------------|
    | Code HTTP Status       |
    |------------------------|
    */
    
    'lbl_code_http_status_200'             => 'HTTP 200 OK. Standard response for successful HTTP requests.',
    'lbl_code_http_status_400'             => 'HTTP Error 400: Standard response for successful HTTP requests.',
    'lbl_code_http_status_401'             => 'HTTP Error 401: Unauthorized access.',  
    'lbl_code_http_status_402'             => 'HTTP Error 402: Requires payment.',
    'lbl_code_http_status_403'             => 'HTTP Error 403: Action prohibited.',
    'lbl_code_http_status_404'             => 'HTTP Error 404: Can not find the requested page.',
    'lbl_code_http_status_407'             => 'HTTP Error 407: Requires proxy authentication.',
    'lbl_code_http_status_415'             => 'HTTP Error 415: Media type not supported.',
    'lbl_code_http_status_500'             => 'HTTP Error 500: Internal Server Error.',  
    'lbl_code_http_status_501'             => 'HTTP Error 501: Action not implemented.',
    'lbl_code_http_status_502'             => 'HTTP Error 502: Gateway wrong.',
    'lbl_code_http_status_503'             => 'HTTP Error 503: Service Unavailable.',           
    'lbl_code_http_status_500_clean_cache' => 'HTTP Error 500. If you have cleared the cache, please refresh the browser please.',

);