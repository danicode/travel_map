<?php 

return array(

    'landing'    => '/',
    'reset'      => '/#/auth/reset',
    'post_reset' => '/auth/reset',
    'login'      => '/auth/login',
    'signup'     => '/auth/signup',
    'logout'     => '/auth/logout',
    'remind'     => '/auth/remind',

);