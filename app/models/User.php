<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
        
        const TABLE_NAME                 = 'users';
        const MODEL_NAME                 = 'User';
        
        const COLUMN_PASSWORD            = 'password';
        const COLUMN_EMAIL               = 'email';
        const COLUMN_FIRST_NAME          = 'first_name';
        const COLUMN_LAST_NAME           = 'last_name';
        const COLUMN_IMAGE_URL           = 'image_url';
        const COLUMN_DESCRIPTION         = 'description';
        const COLUMN_REMEMBER_TOKEN      = 'remember_token';
        const FORM_PASSWORD_CONFIRMATION = 'password_confirmation'; 
        const FORM_TOKEN                 = "token";

        /**
	 * EN: Property specifies which attributes should be mass-assignable. 
         * This can be set at the class or instance level.
         * 
         * ES: Campos deseamos que se puedan insertar en la tabla.
         * Utilizando el metodo "create" asignación en masa y por 
         * medidas de seguridad Laravel nos pide que especifiquemos 
         * la var $fillable.
	 *
	 * @var array
	 */
        protected $fillable = array(User::COLUMN_EMAIL,User::COLUMN_PASSWORD);
        
        /**
         * Get Validation rules to login.
         *
         * @var array 
         */
        public static $rulesLogin = array(
            User::COLUMN_EMAIL    => 'required|email|max:255|exists:users,email',
            User::COLUMN_PASSWORD => 'required|min:8|max:100',
        );
        
        /**
         * Get Validation rules to signup.
         *
         * @var array 
         */
        public static $rulesSignup = array(
            User::COLUMN_EMAIL               => 'required|email|max:255|unique:users,email',
            User::COLUMN_FIRST_NAME          => 'required|max:100',
            User::COLUMN_LAST_NAME           => 'required|max:50',
            User::COLUMN_PASSWORD            => 'required|min:8|max:100',
            User::FORM_PASSWORD_CONFIRMATION => 'required|min:8|max:100|same:password',
        );
        
        /**
         * Get Validation rules to reset password.
         *
         * @var array 
         */
        public static $rulesResetPassword = array(
            User::COLUMN_EMAIL               => 'required|email|max:255|unique:users,email',
            User::COLUMN_PASSWORD            => 'required|min:8|max:100',
            User::FORM_PASSWORD_CONFIRMATION => 'required|min:8|max:100|same:password',
        );
        
        /**
         * Get attributes names to login
         * 
         * @return array
         */
        public static function getAttributeNamesLogin() {
            return array(
                User::COLUMN_EMAIL    => Lang::get('general.email'),
                User::COLUMN_PASSWORD => Lang::get('general.password'),
            );
        }
        
        /**
         * Get attributes names to signup
         * 
         * @return array
         */
        public static function getAttributeNamesSignup() {
            return array(
                User::COLUMN_EMAIL               => Lang::get('general.email'),
                User::COLUMN_FIRST_NAME          => Lang::get('general.first_name'),
                User::COLUMN_LAST_NAME           => Lang::get('general.last_name'),
                User::COLUMN_PASSWORD            => Lang::get('general.password'),
                User::FORM_PASSWORD_CONFIRMATION => Lang::get('general.password_confirmation'),
            );
        }
        
        /**
         * Get attributes names to reset password
         * 
         * @return array
         */
        public static function getAttributeNamesResetPassword() {
            return array(
                User::COLUMN_EMAIL               => Lang::get('general.email'),
                User::COLUMN_PASSWORD            => Lang::get('general.password'),
                User::FORM_PASSWORD_CONFIRMATION => Lang::get('general.password_confirmation'),
            );
        }        
        
        /**
         * Get the error messages to login
         * 
         * @return array
         */
        public static function getErrorMessages() {
            return array(
                'required' => 'email',
                'min'      => 'password',
                'max'      => ''
            );
        }
        
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
            return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
            return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
            return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
            $this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
            return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
            return $this->email;
	}

}