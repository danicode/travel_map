<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
 |------------------|
 | General          |
 |------------------|
 */

Route::get(Lang::get('routes.landing'), 'LandingController@index');

Route::post(Lang::get('routes.login'), array('before' => 'csrf_json', 'uses' => 'AuthController@post_login'));

Route::post(Lang::get('routes.signup'), array('before' => 'csrf_json', 'uses' => 'AuthController@post_signup'));

Route::get(Lang::get('routes.logout'), array('before' => 'auth_rest', 'uses' => 'AuthController@get_logout'));

//Route::group(['before' => 'auth_rest'], function()
//{
//    Route::get(Lang::get('routes.logout'), 'AuthController@get_logout');
//});

Route::group(["before" => "guest_rest"], function()
{            
    Route::get(Lang::get('routes.reset').DIRECTORY_SEPARATOR.'{token}', 'RemindersController@get_reset');
});  

Route::group(array('before' => 'csrf_json'), function()
{
    Route::group(["before" => "guest_rest"], function()
    {
        Route::post(Lang::get('routes.remind'), 'RemindersController@post_remind');

        Route::post(Lang::get('routes.post_reset'), 'RemindersController@post_reset');
    });                

});

// =============================================
// CATCH ALL ROUTE =============================
// =============================================
// all routes that are not home or api will be redirected to the frontend
// this allows angular to route them
App::missing(function($exception)
{
    return Redirect::to(Lang::get('routes.landing'));
});