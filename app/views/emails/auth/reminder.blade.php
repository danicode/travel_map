<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="eng" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Website travel schedule">
    <meta name="keywords"    content="travel,schedule,map,travel map,travel schedule">
    <meta name="author"      content="danicode">

    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{{ Lang::get('general.title') }}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">

    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}" type="image/x-icon" />

    <link rel="stylesheet" href="{{ URL::asset('app/bower_components/bootstrap/dist/css/bootstrap.css') }}" /> 
    <link rel="stylesheet" href="{{ URL::asset('app/css/email_register.css') }}" />
</head>
<body class="email-color">        
    <div class="container"> 
        <div class="text-center">
            <h1 class="title">{{ Lang::get('general.title') }}</h1>
            <h2>{{ Lang::get('general.password_reset') }}</h2>
            {{ Lang::get('general.message_reset_password') }} <a href="{{ URL::to(Lang::get('routes.reset'), array($token)) }}">{{ Lang::get('general.title') }}</a>
        </div>
    </div>
</body>
</html>