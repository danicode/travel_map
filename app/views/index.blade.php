<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="eng" class="no-js" ng-app="app"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="description" content="Website travel schedule">
    <meta name="keywords"    content="travel,schedule,map,travel map,travel schedule">
    <meta name="author"      content="danicode">
    
    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{{'lbl_travel_map' | translate}}</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    
    <link rel="shortcut icon" href="[[[ URL::asset('favicon.ico') ]]]" type="image/x-icon" />
    
    <!-- add lib css -->
    <link rel="stylesheet" href="[[[ URL::asset('app/bower_components/leaflet-dist/leaflet.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/bower_components/leaflet.draw/dist/leaflet.draw.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/bower_components/leaflet-search/dist/leaflet-search.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/bower_components/Leaflet.awesome-markers/dist/leaflet.awesome-markers.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/bower_components/bootstrap/dist/css/bootstrap.css') ]]]" />         
    <link rel="stylesheet" href="[[[ URL::asset('app/bower_components/Font-Awesome/css/font-awesome.css') ]]]" />      
    <link rel="stylesheet" href="[[[ URL::asset('app/bower_components/leaflet-fullscreen/Control.FullScreen.css') ]]]" />
    <!-- end add lib css-->   

    <!-- build:css -->
    <link rel="stylesheet" href="[[[ URL::asset('app/css/main.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/landing.css') ]]]" />    
    <link rel="stylesheet" href="[[[ URL::asset('app/css/header.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/fonts.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/map.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/menu.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/footer.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/transitions.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/main.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/popup.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/form-validate.css') ]]]" />
    <link rel="stylesheet" href="[[[ URL::asset('app/css/responsive.css') ]]]" />
    <!-- endbuild -->
</head>
<body ng-controller="GlobalController">
    
    <!-- header -->
    <header ng-include="includeHeader()"></header> 

    <!-- body -->
    <div id="body" ng-view=""></div>
    
    <!-- footer -->
    <footer class="text-center" ng-include="'[[[ URL::asset('app/partials/footer.html') ]]]'"></footer>
    
    <div class="modal" ng-show="loadingView">
        <ul id="loading">
            <li class="bar" ng-repeat="i in [0,1,2,3,4,5,6,7,8,9]"></li>
        </ul>
    </div>

    <!-- add lib js -->    
    <script src="[[[ URL::asset('app/bower_components/modernizr/modernizr.js') ]]]"></script> 
    <script src="[[[ URL::asset('app/bower_components/es5-shim/es5-shim.js') ]]]"></script> 
    <script src="[[[ URL::asset('app/bower_components/jquery/dist/jquery.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/bootstrap/dist/js/bootstrap.js') ]]]"></script>    
    <script src="[[[ URL::asset('app/bower_components/angular/angular.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/leaflet-dist/leaflet.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-leaflet-directive/dist/angular-leaflet-directive.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/leaflet.draw/dist/leaflet.draw.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/Leaflet.awesome-markers/dist/leaflet.awesome-markers.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-bootstrap/ui-bootstrap.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/leaflet-search/dist/leaflet-search.js') ]]]"></script>        
    <script src="[[[ URL::asset('app/bower_components/leaflet-fullscreen/Control.FullScreen.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-truncate/dist/angular-truncate.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-elastic/elastic.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/kineticjs/kinetic-v5.1.0.js') ]]]"></script>  
    <script src="[[[ URL::asset('app/bower_components/angular-file-upload/angular-file-upload.js') ]]]"></script>     
    <script src="[[[ URL::asset('app/bower_components/angular-resource/angular-resource.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-route/angular-route.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-sanitize/angular-sanitize.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-cookies/angular-cookies.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-translate/angular-translate.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-translate-loader-partial/angular-translate-loader-partial.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js') ]]]"></script>
    <script src="[[[ URL::asset('app/bower_components/angular-translate-storage-local/angular-translate-storage-local.js') ]]]"></script>    
    <script src="[[[ URL::asset('app/bower_components/underscore/underscore.js') ]]]"></script>  
    <script src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>   
    <!-- end add lib js-->
    
    <!-- build:js -->    
    <script src="[[[ URL::asset('app/js/html_libs/tm/tm.utils.responsive.js') ]]]"></script>
    
    <script src="[[[ URL::asset('app/js/app/app.js') ]]]"></script>    
        
    <script src="[[[ URL::asset('app/js/app/controllers/globalController.js') ]]]"></script>  
    <script src="[[[ URL::asset('app/js/app/controllers/landingController.js') ]]]"></script> 
    <script src="[[[ URL::asset('app/js/app/controllers/homeController.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/controllers/translateController.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/controllers/accordionDemoController.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/controllers/forgotPasswordPopupController.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/controllers/notificationController.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/controllers/progressbarController.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/controllers/resetPasswordPopupController.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/controllers/otherwiseController.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/controllers/userController.js') ]]]"></script>
    
    <script src="[[[ URL::asset('app/js/app/directives/ng-pw-check.js') ]]]"></script>
    
    <script src="[[[ URL::asset('app/js/app/services/popupModalService.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/services/popupModalProgressbarService.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/services/flashService.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/services/sessionService.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/services/cookieService.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/services/authenticationService.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/services/handlerHttpService.js') ]]]"></script>
    <script src="[[[ URL::asset('app/js/app/services/base64Service.js') ]]]"></script>
    <!-- endbuild -->

    <script>        
        angular.module("app").constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');  
    </script>        
    
</body>
</html>