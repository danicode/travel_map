'use strict';

/* App Module */

var app = angular.module('app', [
    'ui.bootstrap',
    'leaflet-directive', 
    'ngRoute',
    'ngResource',
    'ngSanitize',
    'ngCookies',
    'pascalprecht.translate',
    'truncate',    
    
    'globalController',
    'landingController',   
    'homeController',  
    'translateController',
    'accordionDemoController',
    'forgotPasswordPopupController',
    'notificationController',
    'progressbarController',
    'resetPasswordPopupController',
    'otherwiseController',
    'userController',
    
    'ngPwCheck',
    
    'popupModalService',
    'popupModalProgressbarService',
    'flashService',
    'sessionService',
    'cookieService',
    'authenticationService',
    'handlerHttpService',
    'base64Service'
    
]);

/* Routes */

app.config(['$routeProvider' , '$locationProvider',function($routeProvider, $locationProvider){
    $routeProvider  
        .when('/', {      
            redirectTo :'/landing'
        })
        
        .when('/landing', {      
            templateUrl:'app/partials/landing.html', 
            controller: 'LandingController'
        })       

        .when('/home', {
            templateUrl:'app/partials/home.html', 
            controller: 'HomeController'            
        })

        .when('/auth/reset/:token', {      
            templateUrl:'app/partials/landing.html', 
            controller: 'LandingController'
        })
        
        .otherwise({ 
            templateUrl:'app/partials/landing.html', 
            controller: 'OtherwiseController'
        });  
}]);

/* Providers */
    
app.config(['$httpProvider', function($httpProvider){

    var interceptor = [
        '$q', 
        'SessionService',
        
        function($q, SessionService) {
 
            var success = function(response){
                return response;
            };

            var error = function(response){
                if (response.status === 401) {
                    SessionService.unset('authenticated');
                }
                return $q.reject(response);

            };

            return function(promise){
                return promise.then(success, error);
            };        
    }];

    $httpProvider.responseInterceptors.push(interceptor);
    
}]);

/* Translate */

app.config([
    '$translateProvider', 
    '$translatePartialLoaderProvider', 
    
    function ($translateProvider, 
        $translatePartialLoaderProvider) {    
            
        console.log("execute config $translateProvider");
        
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '/app/assets/translation/{lang}/{part}.json'
        });

        $translateProvider.preferredLanguage('gb');
        $translateProvider.fallbackLanguage('gb');

        // Local storage does not work then  uses cookie theoretically
        $translateProvider.useLocalStorage();
    
}]);

/* Execute */     

// Only if it's logged can access the other parts of the site.
app.run([
    '$rootScope',
    'AuthenticationService',
    'PopupModalService',
    'CookieService',
    '$filter',
    '$location',
    
    function($rootScope, AuthenticationService, PopupModalService, CookieService, $filter, $location) { 
        
        console.log("execute run!");
        console.log("$location.path(): "+$location.path());

        $rootScope.globals = {};
        
        $rootScope.USER_KEY = 'current_user';
        
        //200, 400, 
        $rootScope.CODE_HTTP_STATUS   = [401, 402, 403, 404, 405, 407, 415, 500, 501, 502, 503]; 
        
        // Global variable.  
        $rootScope.COUNTRY_LIST   = ['es','gb']; 

        // Menu from website.
        $rootScope.HOME_MENU      = 'home';
        $rootScope.SIGNUP_MENU    = 'signup';
        $rootScope.ABOUT_MENU     = 'about';
        $rootScope.LANGUAGE_MENU  = 'language';
        $rootScope.PRIVACITY_MENU = 'privacity';

        // Menu list.
        $rootScope.ELEMENT_MENU = {
            'home'      : 0,
            'signup'    : 1,
            'about'     : 2,
            'language'  : 3,
            'privacity' : 4
        };             
        
        // Pages from web site.
        $rootScope.HOME_PAGE    = 'home';
        $rootScope.LANDING_PAGE = 'landing';
        $rootScope.BLANK_PAGE   = '';
        $rootScope.ROOT_PAGE    = 'http://travel_map.local/';
        
        //console.log("CookieService: "+jQuery.isEmptyObject(CookieService.get($rootScope.USER_KEY)));
        
        console.log("$location");
        console.log($location);
  
        if(!jQuery.isEmptyObject(CookieService.get($rootScope.USER_KEY))) {                
            AuthenticationService.setCacheSession(CookieService.get($rootScope.USER_KEY));
            AuthenticationService.restoreAccount();
            $location.path('/'+$rootScope.HOME_PAGE);           
        } else {              
            if(AuthenticationService.isLoggedIn()) {      
                AuthenticationService.restoreAccount();
            } 
        }
        
        // Routes that require auth.
        $rootScope.ROUTES_THAT_REQUIRE_AUTH = ['/'+$rootScope.HOME_PAGE];
        
        // Routes available.
        $rootScope.ROUTES_AVAILABLE = ['/', '/'+$rootScope.LANDING_PAGE, '/'+$rootScope.HOME_PAGE, $rootScope.ROOT_PAGE];

        $rootScope.$on('$locationChangeStart',function(event, next, current) {
            
            console.log("execute change start!");          
            
            // Get the path.
            var path = next.toLocaleString();
            
            console.log("path: "+path);

            // Remove the root of the path, just need the section.
            path = path.replace($rootScope.ROOT_PAGE+'#','');
            //path = path.replace($rootScope.ROOT_PAGE,'');
            console.log("lala path: "+path);

            // Verify if the section is auth and if it's logged.
            if(_($rootScope.ROUTES_THAT_REQUIRE_AUTH).contains(path) && !AuthenticationService.isLoggedIn()) {   
                var modal = {
                    templateUrl : 'app/popup/notication-popup.html',
                    windowClass : 'custom-modal',
                    controller  : 'NotificationController',
                    backdrop    : 'true',
                    size        : '',
                    title       : $filter('translate')('lbl_notification'),
                    notification: $filter('translate')('lbl_restrict_access'),
                    error       : true
                };
                
                PopupModalService.show(modal);
                event.preventDefault();
            }   
            
            // When the user try go to landing page from to home page.
            if(path === '/'+$rootScope.LANDING_PAGE && AuthenticationService.isLoggedIn() && current !== next) {                
                var modal = {
                    templateUrl : 'app/popup/notication-popup.html',
                    windowClass : 'custom-modal',
                    controller  : 'NotificationController',
                    backdrop    : 'true',
                    size        : '',
                    title       : $filter('translate')('lbl_notification'),
                    notification: $filter('translate')('lbl_restrict_access_landing'),
                    error       : true
                };
                
                PopupModalService.show(modal);
                event.preventDefault();
            }
        });
        
//        $rootScope.$on('$routeChangeStart', function(e, curr, prev) { 
//            console.log("alalalaalalalala 1");
//            console.log(curr);
//            if (curr.$$route && curr.$$route.resolve) {
//                // Show a loading message until promises are not resolved
//                console.log("alalalaalalalala 2");
//                $rootScope.loadingView = true;
//            }
//            $rootScope.loadingView = true;
//        });
//
//        $rootScope.$on('$routeChangeSuccess', function(e, curr, prev) { 
//            // Hide loading message
//            $rootScope.loadingView = false;
//        });
}]);