'use strict';

/* Controllers */

var forgotPasswordPopupController = angular.module('forgotPasswordPopupController', []);

forgotPasswordPopupController.controller("ForgotPasswordPopupController", [
    '$scope', 
    '$modalInstance',  
    'PopupModalProgressbarService',
    'AuthenticationService',
    function($scope, $modalInstance, PopupModalProgressbarService, AuthenticationService) {
        
        $scope.credentials = { 
            email: ''
        }; 

        $scope.send = function() {
            var modal = {
                templateUrl : 'app/popup/progressbar-popup.html',
                windowClass : 'custom-modal',
                controller  : 'ProgressbarController',
                backdrop    : 'static',
                size        : '',
                title       : '',
                notification: '',
                error       : false
            };

            PopupModalProgressbarService.show(modal);
            
            AuthenticationService.remind($scope.credentials).success(function() {
                PopupModalProgressbarService.close();
                //$location.path('/'+$rootScope.HOME_PAGE);
            }).error(function() {
                PopupModalProgressbarService.close();
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };                    
    }
]);