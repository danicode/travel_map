'use strict';

/* Controllers */

var globalController = angular.module('globalController', []);

globalController.controller('GlobalController', [
    '$scope', 
    'AuthenticationService',
    '$location',
    '$rootScope',
    
    function($scope, AuthenticationService, $location, $rootScope) {                
        $scope.includeHeader = function() {     
            console.log("execute includeHeader");
            //return (AuthenticationService.isLoggedIn()) ? 'app/partials/header_login.html' : 'app/partials/header.html'; 
            var partials = 'app/partials/header.html';
            
            if(AuthenticationService.isLoggedIn()) {
                partials = 'app/partials/header_login.html';
            } else {
                if($location.path() === '/'+$rootScope.HOME_PAGE) {
                    $location.path('/'+$rootScope.LANDING_PAGE); 
                }                
            }
            
            return partials;
        };
        
        $scope.logout = function() {
            console.log("execute logout");        
            AuthenticationService.logout().success(function() {
                $location.path('/'+$rootScope.LANDING_PAGE);                
            });
        };
}]);