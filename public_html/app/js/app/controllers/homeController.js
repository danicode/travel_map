'use strict';

/* Controllers */

var homeController = angular.module('homeController', []);

homeController.controller('HomeController', [
    '$scope', 
    'leafletData', 
    '$compile',
    '$translate',
    '$translatePartialLoader',
    '$rootScope',
    
    function($scope, leafletData, $compile, $translate, $translatePartialLoader, $rootScope) {
        
        $translatePartialLoader.addPart('translation');
        $translate.refresh();
        
        $scope.geocoder = new google.maps.Geocoder();

        $scope.googleGeocoding = function (text, callResponse)
        {
            $scope.geocoder.geocode({address: text}, callResponse);
        };

        $scope.filterJSONCall = function (rawjson)
        {
            var json = {},
            key, loc, disp = [];

            for(var i in rawjson)
            {
                key = rawjson[i].formatted_address;

                loc = L.latLng( rawjson[i].geometry.location.lat(), rawjson[i].geometry.location.lng() );

                json[ key ]= loc;	//key,value format
            }

            return json;
        };            
        
        $scope.center = {
            lat: 40.8471,
            lng: 14.0625,
            zoom: 2                    
        };                

        $scope.centerMap = function() {                    
            leafletData.getMap().then(function(map) {
                map.panTo([51.505, -0.09]);
            });
        };                            

        //control draw

        //var donde se va a guardar el marker o liner seleccionado
        $scope.marker = null;
        $scope.liner  = null;
        
        $scope.homeLayer  = null;
        $scope.heartLayer = null;
        $scope.starLayer  = null;

        //var flag de makers, liners y todos items
        $scope.activateHideMarker = false;
        $scope.activateHideLiner  = false;
        $scope.activateHideItems  = false;

        $scope.toolbarEditMode = false;

        $scope.markerLayers = {
            features: [],
            type: "FeatureCollection"
        };

        $scope.linerLayers = {
            features: [],
            type: 'FeatureCollection'
        };

        $scope.drawnItems = new L.FeatureGroup();

        var CustomMarker = L.AwesomeMarkers.icon({
            icon:               'home',
            markerColor:        'blue',
            title:              '',
            description:        '',
            index:              0,
            selected:           false,
            address_components: [],
            img_url:            'app/img/tiger.jpg',
            date:               '' 
        });

        $scope.markerHovers = { 
            home:  '#35A7DB', 
            heart: '#E74A37',
            star:  '#F38E2E'
        };
        
        $scope.markerColors = { 
            home:  'blue', 
            heart: 'red',
            star:  'orange'
        };

        $scope.drawControl = new L.Control.Draw({
            position: 'topleft',
            draw: {
                polyline: {
                    metric: true,
                    drawError: {
                        color:   '#e1e100', // Color the shape will turn when intersects
                        message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
                    },
                    shapeOptions: {
                        color:         'blue',
                        index:         0,
                        message:       '',
                        selected:      false,
                        previousColor: ''
                    }
                },
                polygon:   false,
                rectangle: false,
                circle:    false, // Turns off this drawing tool
                marker: {
                    icon: L.AwesomeMarkers.icon({
                        icon:               'home',
                        markerColor:        'blue',
                        title:              '',
                        description:        '',
                        index:              0,
                        selected:           false,
                        address_components: [],
                        img_url:            'app/img/tiger.jpg',
                        date:               ''
                    })                        
                }
            },
            edit: {
                featureGroup: $scope.drawnItems
            }
        });     

        //Config draw control
        L.drawLocal.draw.toolbar.buttons.marker   = 'add marker';
        L.drawLocal.draw.toolbar.buttons.polyline = 'add route';                
        //L.drawLocal.edit.toolbar.buttons.edit = 'Edit layers.';
        //L.drawLocal.edit.toolbar.buttons.editDisabled = 'No layers to edit.';
        //L.drawLocal.edit.toolbar.buttons.remove = 'Delete layers.';
        //L.drawLocal.edit.toolbar.buttons.removeDisabled = 'No layers to delete.';

        leafletData.getMap().then(function(map) {
            map.addControl( new L.Control.Search({
                callData:       $scope.googleGeocoding,
                filterJSON:     $scope.filterJSONCall,
                markerLocation: false,
                circleLocation: false,
                autoType:       false,
                autoCollapse:   true,
                minLength:      2,
                zoom:           8
            }));

            console.log("map");
            console.log(map);

            map.options.fullscreenControl = true;

            map.addControl(L.control.fullscreen({
                position: 'topleft', // change the position of the button can be topleft, topright, bottomright or bottomleft, defaut topleft
                //title: 'Show me the fullscreen !', // change the title of the button, default Full Screen
                forceSeparateButton: true, // force seperate button to detach from zoom buttons, default false
                forcePseudoFullscreen: true // force use of pseudo full screen even if full screen API is available, default false
            }));

            map.on('enterFullscreen', function(){
                console.log('entered fullscreen');
            });

            map.on('exitFullscreen', function(){
                console.log('exited fullscreen');
            });

            map.addLayer($scope.drawnItems);
            map.addControl($scope.drawControl);

            map.on('draw:created', function (e) {
                console.log('draw:created');
                
                
                var type = e.layerType, 
                     layer = e.layer;

                console.log(JSON.stringify(layer.toGeoJSON()));
                console.log("layer created");
                console.log(layer);       

                var customLayer = layer.toGeoJSON();

                if (type === 'marker') {
                    $scope.activateHideMarker = true;          

                    if($scope.marker !== null && $scope.marker.options.icon.options.selected){
                        $scope.marker.closePopup();
                        $scope.selectMarker(false);
                    }

                    $scope.marker = layer; 

                    $scope.marker.options.icon.options.index = $scope.markerLayers.features.length;
                    
                    if (!Date.now) {
                        Date.now = function now() {
                            return new Date().getTime();
                        };
                    }
                    
                    var date = Date.now();
                    
                    //$scope.marker.options.icon.options.date = $filter('date')(date, "dd-MMMM-yyyy");
                    
                    $scope.marker.options.icon.options.date = date;

                    $scope.codeLatLng($scope.marker.getLatLng());                    

                    //customLayer.properties.icon = CustomMarker;      
                    
                    customLayer.properties.icon = L.AwesomeMarkers.icon({
                        icon:               'home',
                        markerColor:        'blue',
                        title:              '',
                        description:        '',
                        index:              $scope.markerLayers.features.length,
                        selected:           false,
                        address_components: [],
                        img_url:            'app/img/tiger.jpg',
                        date:               $scope.marker.options.icon.options.date
                    });   

                    //layer.setIcon(CustomMarker);          
                    
                   /*layer.setIcon(L.AwesomeMarkers.icon({
                        icon:              'home',
                        markerColor:       'blue',
                        title:             '',
                        description:       '',
                        index:             0,
                        selected:          false,
                        formatted_address: '',
                        img_url:           'app/img/tiger.jpg',
                        date:              date
                    }));   */       

                    $scope.markerLayers.features.push(customLayer);   

                    var newScope = $scope.$new();

                    var e = $compile('<div popup></div>')(newScope);
                    $scope.marker.bindPopup(e[0]);

                    layer.addEventListener('click', function(){
                        console.log("click on icon"); 

                        if($scope.marker !== null && $scope.marker.options.icon.options.selected) {
                            $scope.marker.closePopup();
                        }

                        if(!$scope.toolbarEditMode) {

                            if($scope.liner !== null && $scope.liner.options.selected) {
                                $scope.selectLiner(false);
                                $scope.liner = null;
                            }

                            if($scope.marker === null) {
                                $scope.marker = layer;                
                                $scope.selectMarker(true);
                            } else {

                                if($scope.marker === layer) {
                                    if($scope.marker.options.icon.options.selected) {                                                
                                        $scope.selectMarker(false);
                                        $scope.marker = null;

                                    } else {

                                        $scope.selectMarker(true);
                                    }
                                } else {

                                    if($scope.marker.options.icon.options.selected) {

                                        $scope.selectMarker(false);
                                        $scope.marker = layer;
                                        $scope.selectMarker(true);

                                    } else {

                                        $scope.marker = layer;                                                    
                                        $scope.selectMarker(true);
                                    }

                                }

                            }

                        }

                    });                            

                }

                if(type === 'polyline') {  

                    $scope.activateHideLiner = true;
                    customLayer.properties.color    = layer.options.color;
                    customLayer.properties.index    = $scope.linerLayers.features.length;
                    customLayer.properties.message  = layer.options.message;
                    customLayer.properties.selected = false;
                    customLayer.previousColor       = '';

                    layer.bindPopup("write a message...");
                    layer.options.index = $scope.linerLayers.features.length;    

                    if($scope.liner !== null && $scope.liner.options.selected){
                        $scope.selectLiner(false);
                    }

                    $scope.linerLayers.features.push(customLayer);

                    $scope.drawnItems.addLayer(layer);


                    if($scope.marker !== null) {          
                        $scope.selectMarker(false);
                        $scope.marker = null;
                    }                            

                    $scope.liner = layer;
                    $scope.selectLiner(true);
                    $scope.liner.openPopup();

                    layer.addEventListener('click', function(){
                        console.log("click on liner");        

                        if($scope.liner !== null && $scope.liner.options.selected) {
                            $scope.liner.closePopup();
                        }

                        if(!$scope.toolbarEditMode) {

                            if($scope.marker !== null && $scope.marker.options.icon.options.selected) {
                                $scope.selectMarker(false);
                                $scope.marker = null;
                            }

                            if($scope.liner === null) {
                                $scope.liner = layer;
                                $scope.selectLiner(true);
                            } else {

                                if($scope.liner === layer) {
                                    if($scope.liner.options.selected) {

                                        $scope.selectLiner(false);
                                        $scope.liner = null;
                                    } else {

                                        $scope.selectLiner(true);
                                    }
                                } else {

                                    if($scope.liner.options.selected) {

                                        $scope.selectLiner(false);
                                        $scope.liner = layer;
                                        $scope.selectLiner(true);

                                    } else {

                                        $scope.liner = layer;
                                        $scope.selectLiner(true);
                                    }

                               }

                           }
                        }

                    });    

                }                      
                $scope.drawnItems.addLayer(layer);
                
                console.log("$scope.markerLayers");
                console.log($scope.markerLayers);

                layer.addEventListener('popupopen', function(){
                    console.log('popupopen');                                
                }); 

            });

            map.on('draw:edited', function (e) {
                var layers = e.layers;
                console.log('draw:edited');                    

                layers.eachLayer(function (layer) {                            
                    //do whatever you want, most likely save back to db

                    var customLayer = layer.toGeoJSON(); 

                    if(customLayer.geometry.type === 'Point') {                                 

                        for(var i = 0; i < $scope.markerLayers.features.length; i++) {                                  

                            if(layer.options.icon.options.index === $scope.markerLayers.features[i].properties.icon.options.index) { 

                                if(layer.options.icon.options.selected) {
                                    $scope.markerLayers.features[i].properties.icon.options.markerColor = layer.options.icon.options.previousColor;
                                    $scope.markerLayers.features[i].properties.icon.options.iconColor = 'white';
                                }

                                console.log("index edited: "+layer.options.icon.options.index);
                                $scope.markerLayers.features[i].geometry.coordinates[0]                    = customLayer.geometry.coordinates[0];
                                $scope.markerLayers.features[i].geometry.coordinates[1]                    = customLayer.geometry.coordinates[1];                                        
                                $scope.markerLayers.features[i].properties.icon.options.icon               = layer.options.icon.options.icon;
                                $scope.markerLayers.features[i].properties.icon.options.title              = layer.options.icon.options.title; 
                                $scope.markerLayers.features[i].properties.icon.options.description        = layer.options.icon.options.description; 
                                $scope.markerLayers.features[i].properties.icon.options.selected           = false;
                                $scope.markerLayers.features[i].properties.icon.options.address_components = layer.options.icon.options.address_components;
                                $scope.markerLayers.features[i].properties.icon.options.img_url            = layer.options.icon.options.img_url;
                                $scope.markerLayers.features[i].properties.icon.options.date               = layer.options.icon.options.date;

                            }
                        }

                    }

                    if(customLayer.geometry.type === 'LineString') {  

                        for(var i = 0; i < $scope.linerLayers.features.length; i++) {                                  

                            if(layer.options.index === $scope.linerLayers.features[i].properties.index) {     

                                if(layer.options.selected) {
                                    $scope.linerLayers.features[i].properties.color = layer.options.previousColor;
                                }

                                $scope.linerLayers.features[i].geometry.coordinates[0][0] = customLayer.geometry.coordinates[0][0];
                                $scope.linerLayers.features[i].geometry.coordinates[0][1] = customLayer.geometry.coordinates[0][1];
                                $scope.linerLayers.features[i].geometry.coordinates[1][0] = customLayer.geometry.coordinates[1][0];
                                $scope.linerLayers.features[i].geometry.coordinates[1][1] = customLayer.geometry.coordinates[1][1];

                                $scope.linerLayers.features[i].properties.message       = layer.options.message;  
                                $scope.linerLayers.features[i].properties.selected      = false;
                                $scope.linerLayers.features[i].properties.previousColor = '';

                            }
                        }

                    }

                });
            });

            map.on('draw:editstart', function (e) {
                console.log("draw:editstart"); 

                $scope.toolbarEditMode = true;                  
            });

            map.on('draw:editstop', function (e) {
                console.log('draw:editstop'); 
                $scope.toolbarEditMode = false; 

                console.log("e");
                console.log(e);

                if(e.handler === 'remove') {
                    if($scope.marker !== null && $scope.marker.options.icon.options.selected) {
                        $scope.selectMarker(false);
                        $scope.marker = null;
                    }                        

                    if($scope.liner !== null && $scope.liner.options.selected) {
                        $scope.selectLiner(false);
                        $scope.liner = null;
                    }      
                }
            });

            map.on('draw:deleted', function (e) {
                console.log("draw:deleted"); 
                var layers = e.layers;
                layers.eachLayer(function (layer) {

                    var customLayer = layer.toGeoJSON();                         

                    if(customLayer.geometry.type === 'Point') { 
                        var index = layer.options.icon.options.index;
                        if (index > -1) {
                            console.log("index: "+index);
                            $scope.markerLayers.features.splice(index, 1);
                        }

                        if($scope.markerLayers.features.length < 1) {
                            $scope.activateHideMarker = false;
                        }
                    }

                    if(customLayer.geometry.type === 'LineString') { 
                        var index = layer.options.index;
                        if (index > -1) {
                            $scope.linerLayers.features.splice(index, 1);
                        }

                        if($scope.linerLayers.features.length < 1) {
                            $scope.activateHideLiner = false;
                        }
                    }

                });
            });

            map.on('draw:drawstop', function (e) {
                console.log("draw:drawstop");

            });

        });      

        $scope.addMarkers = function () {                        
            $scope.activateHideMarker = true;
            leafletData.getMap().then(function(map) {
                L.geoJson($scope.markerLayers,{
                    onEachFeature: function (feature, layer) {
                        
                       layer.feature.properties.icon.options.markerColor = $scope.markerColors[layer.feature.properties.icon.options.icon];
                       
                       console.log("addMarkers - layer");
                       console.log(layer);

                        layer.setIcon(layer.feature.properties.icon);
                        
                        var newScope = $scope.$new();

                        var e = $compile('<div popup></div>')(newScope);
                        layer.bindPopup(e[0]);

                        layer.addEventListener('click', function(){
                            console.log("click on icon");

                            if(!$scope.toolbarEditMode) {
                                if($scope.liner !== null && $scope.liner.options.selected) {
                                    $scope.selectLiner(false);
                                    $scope.liner = null;
                                }

                                if($scope.marker === null) {
                                    $scope.marker = layer;
                                    $scope.selectMarker(true);
                                } else {

                                    if($scope.marker === layer) {
                                        if($scope.marker.options.icon.options.selected) {

                                            $scope.selectMarker(false);
                                            $scope.marker = null;
                                        } else {

                                            $scope.selectMarker(true);
                                        }
                                    } else {

                                        if($scope.marker.options.icon.options.selected) {

                                            $scope.selectMarker(false);
                                            $scope.marker = layer;
                                            $scope.selectMarker(true);
                                        } else {
                                            $scope.marker = layer;
                                            $scope.selectMarker(true);
                                        }

                                    }

                                }
                            }

                        });

                        /*if(layer.options.icon.options.message === "") {
                            layer.bindPopup("write a message...");
                        } else {
                            layer.bindPopup(layer.options.icon.options.message);
                        }*/

                        $scope.drawnItems.addLayer(layer);
                    }
                });
                map.addLayer($scope.drawnItems);
            });       
        };

        $scope.addLiners = function () {     
            $scope.activateHideLiner = true;
            leafletData.getMap().then(function(map) {
                L.geoJson($scope.linerLayers,{
                    onEachFeature: function (feature, layer) {

                        var layerJSON = layer.toGeoJSON();

                        layer.options.color =  layer.feature.properties.color;
                        layer.options.index = layerJSON.properties.index;
                        layer.options.message = layerJSON.properties.message;

                        layer.addEventListener('click', function(){
                            console.log("click on liner");

                            if(!$scope.toolbarEditMode) {
                                if($scope.marker !== null && $scope.marker.options.icon.options.selected) {
                                    $scope.selectMarker(false);
                                    $scope.marker = null;
                                }                                        

                                if($scope.liner === null) {
                                    $scope.liner = layer;
                                    $scope.selectLiner(true);
                                } else {

                                    if($scope.liner === layer) {
                                        if($scope.liner.options.selected) {

                                            $scope.selectLiner(false);
                                            $scope.liner = null;
                                        } else {

                                            $scope.selectLiner(true);
                                        }
                                    } else {

                                        if($scope.liner.options.selected) {

                                            $scope.selectLiner(false);
                                            $scope.liner = layer;
                                            $scope.selectLiner(true);

                                        } else {

                                            $scope.liner = layer;
                                            $scope.selectLiner(true);
                                        }

                                   }

                               }
                            }
                        });

                        console.log("addItems layerJSON");
                        console.log(layerJSON);

                        if(layer.options.message === "") {
                            layer.bindPopup("write a message...");
                        } else {
                            layer.bindPopup(layer.options.message);
                        }                                  

                        $scope.drawnItems.addLayer(layer);
                    }
                });
                map.addLayer($scope.drawnItems);
            });       
        };

        /**
         * Cambiar el color de la linea se debe recorrer las capas y encontrar
         * la misma capa q tiene la linea q se desea modificar, para ello 
         * su index de ser igual
         * @param {string} color atributo del objeto linea. 
         */
        $scope.changeLineColor = function(color) {
            console.log("change color line");

            for(var i = 0; i < $scope.linerLayers.features.length; i++) {

                if($scope.linerLayers.features[i].properties.index === $scope.liner.options.index) {
                    $scope.linerLayers.features[i].properties.color = color;                            
                }                   
            }

            $scope.liner.options.previousColor = color;
        };

        /**
         * Cambiar el icono, primero se debe crear una instancia de L.AwesomeMarkers.icon
         * con el icono q se desea cambiar q es el q se pasa por parametro
         * luego se debe recorrer las capas hasta encontrar la capa por su index
         * y por ultimo settear el icono al marcador
         * @param {string} icon atributo del objeto marcador. 
         */
        $scope.changeIcon = function(icon) {
            console.log("change icon");
            
            var CustomMarker = L.AwesomeMarkers.icon({
                icon:               icon,
                iconColor:          $scope.markerHovers[icon],
                markerColor:        $scope.marker.options.icon.options.markerColor,
                title:              $scope.marker.options.icon.options.title,
                index:              $scope.marker.options.icon.options.index,
                description:        $scope.marker.options.icon.options.description,
                selected:           true,
                address_components: $scope.marker.options.icon.options.address_components,
                img_url:            $scope.marker.options.icon.options.img_url,
                date:               $scope.marker.options.icon.options.date
            }); 
            
            $scope.marker.setIcon(CustomMarker);
            
            $scope.changeMarker('icon');

            /*for(var i = 0; i < $scope.markerLayers.features.length; i++) {

                if($scope.markerLayers.features[i].properties.icon.options.index === $scope.marker.options.icon.options.index) {
                    $scope.markerLayers.features[i].properties.icon.options.icon = icon;
                }
            }
            console.log("$scope.markerLayers");
            console.log($scope.markerLayers);*/
            
        };
        
        /**
         * Este metodo se utiliza para guardar los cambios en el array.  
         * @param {string} attribute atributo del objeto marcador. 
         */
        $scope.changeMarker = function(attribute) {
            for(var i = 0; i < $scope.markerLayers.features.length; i++) {

                if($scope.markerLayers.features[i].properties.icon.options.index === $scope.marker.options.icon.options.index) {
                    $scope.markerLayers.features[i].properties.icon.options[attribute] = $scope.marker.options.icon.options[attribute];
                }
            }   
        };

        /**
         * cambia el mensaje de la ruta(linea). Si no tiene mensaje, coloco
         * el mensaje por defecto q es "write a message" sino el q se agrego.
         */
        $scope.changeMessageLiner = function () {
            if($scope.liner.options.message === "") {
                $scope.liner._popup.setContent("write a message...");                        
            } else {
                $scope.liner._popup.setContent($scope.liner.options.message);                        
            }      

            for(var i = 0; i < $scope.linerLayers.features.length; i++) {

                if($scope.linerLayers.features[i].properties.index === $scope.liner.options.index) {
                    $scope.linerLayers.features[i].properties.message = $scope.liner.options.message;                     
                }                   
            }
        }; 

        /**
         * captura el evento click sobre el mapa, lo que hago
         * es si el marcador esta seleccionado lo deselecciono
         * @param {obj} event es el objeto del evento generado. 
         */
        $scope.$on('leafletDirectiveMap.click', function(event){                    
            console.log("event name: "+event.name);

            if($scope.marker !== null) {
                if($scope.marker.options.icon.options.selected) {                            
                    $scope.selectMarker(false);                            
                    $scope.marker = null;
                }
            }

            if($scope.liner !== null) {
                if($scope.liner.options.selected) {
                    $scope.selectLiner(false);
                    $scope.liner = null;
                }                        
            }
        });

        /**
         * Se utiliza para hacer el efecto de seleccion de un marcador.
         * 
         * @param selection boolean. true -> to select and false to unselect
         */
        $scope.selectMarker = function(selection) {
            console.log('selectMarker - selection param: '+selection);
            var icon = null;
            if(selection) {

                icon = L.AwesomeMarkers.icon({
                    icon:               $scope.marker.options.icon.options.icon,
                    iconColor:          $scope.markerHovers[$scope.marker.options.icon.options.icon],
                    markerColor:        'white',
                    title:              $scope.marker.options.icon.options.title,
                    index:              $scope.marker.options.icon.options.index,
                    description:        $scope.marker.options.icon.options.description,
                    selected:           selection,
                    address_components: $scope.marker.options.icon.options.address_components,
                    img_url:            $scope.marker.options.icon.options.img_url,
                    date:               $scope.marker.options.icon.options.date
                });
            } else {
                icon = L.AwesomeMarkers.icon({
                    icon:               $scope.marker.options.icon.options.icon,
                    iconColor:          'white',
                    markerColor:        $scope.markerColors[$scope.marker.options.icon.options.icon],
                    title:              $scope.marker.options.icon.options.title,
                    index:              $scope.marker.options.icon.options.index,
                    description:        $scope.marker.options.icon.options.description,
                    selected:           selection,
                    address_components: $scope.marker.options.icon.options.address_components,
                    img_url:            $scope.marker.options.icon.options.img_url,
                    date:               $scope.marker.options.icon.options.date
                });
            }
            
            $scope.marker.setIcon(icon);
        };

        /**
         * Se utiliza para hacer el efecto de seleccion de una linea.
         * 
         * @param selection boolean. true -> to select and false to unselect
         */
        $scope.selectLiner = function(selection) {
            if(selection) {
                $scope.liner.options.previousColor = $scope.liner.options.color;
                $scope.liner.setStyle({color: '#FFFF00'});
                $scope.liner.setStyle({opacity: 1});
                $scope.liner.options.selected = selection;      
                $scope.liner.options.color = '#FFFF00';
            } else {
                console.log($scope.liner);
                $scope.liner.setStyle({color: $scope.liner.options.previousColor});                   
                $scope.liner.options.color = $scope.liner.options.previousColor;
                $scope.liner.options.selected = selection;
                $scope.liner.options.previousColor = '';
                $scope.liner.setStyle({opacity: 0.5});
            }  
        };    

        $scope.addAllItems = function() {             

            /*console.log("$scope.activateHideLiner: "+$scope.activateHideLiner);
            console.log("$scope.activateHideMarker: "+$scope.activateHideMarker);*/
            leafletData.getMap().then(function(map) {
                console.log("!$scope.activateHideLiner: "+!$scope.activateHideLiner);
                console.log("$scope.linerLayers.features: "+($scope.linerLayers.features.length > 0));
                console.log("if(!$scope.activateHideLiner &&  $scope.linerLayers.features.length > 0): "+(!$scope.activateHideLiner &&  $scope.linerLayers.features.length > 0));
                if(!$scope.activateHideLiner &&  $scope.linerLayers.features.length > 0) {
                    L.geoJson($scope.linerLayers,{
                        onEachFeature: function (feature, layer) {

                            var layerJSON = layer.toGeoJSON();

                            layer.options.color =  layer.feature.properties.color;
                            layer.options.index = layerJSON.properties.index;
                            layer.options.message = layerJSON.properties.message;

                            layer.addEventListener('click', function(){
                                console.log("click on liner");

                                if(!$scope.toolbarEditMode) {
                                    if($scope.marker !== null && $scope.marker.options.icon.options.selected) {
                                        $scope.selectMarker(false);
                                        $scope.marker = null;
                                    }                                        

                                    if($scope.liner === null) {
                                        $scope.liner = layer;
                                        $scope.selectLiner(true);
                                    } else {

                                        if($scope.liner === layer) {
                                            if($scope.liner.options.selected) {

                                                $scope.selectLiner(false);
                                                $scope.liner = null;
                                            } else {

                                                $scope.selectLiner(true);
                                            }
                                        } else {

                                            if($scope.liner.options.selected) {

                                                $scope.selectLiner(false);
                                                $scope.liner = layer;
                                                $scope.selectLiner(true);

                                            } else {

                                                $scope.liner = layer;
                                                $scope.selectLiner(true);
                                            }

                                       }

                                   }
                                }
                            });

                            console.log("addItems layerJSON");
                            console.log(layerJSON);

                            if(layer.options.message === "") {
                                layer.bindPopup("write a message...");
                            } else {
                                layer.bindPopup(layer.options.message);
                            }                                  

                            $scope.drawnItems.addLayer(layer);
                        }
                    });
                }

                if(!$scope.activateHideMarker && $scope.markerLayers.features.length > 0) {

                    L.geoJson($scope.markerLayers,{
                        onEachFeature: function (feature, layer) {

                            layer.setIcon(layer.feature.properties.icon);

                            layer.addEventListener('click', function(){
                                console.log("click on icon");

                                console.log($scope.marker);                          

                                if(!$scope.toolbarEditMode) {
                                    if($scope.liner !== null && $scope.liner.options.selected) {
                                        $scope.selectLiner(false);
                                        $scope.liner = null;
                                    }

                                    if($scope.marker === null) {
                                        $scope.marker = layer;
                                        $scope.selectMarker(true);
                                    } else {

                                        if($scope.marker === layer) {
                                            if($scope.marker.options.icon.options.selected) {

                                                $scope.selectMarker(false);
                                                $scope.marker = null;
                                            } else {

                                                $scope.selectMarker(true);
                                            }
                                        } else {

                                            if($scope.marker.options.icon.options.selected) {

                                                $scope.selectMarker(false);
                                                $scope.marker = layer;
                                                $scope.selectMarker(true);
                                            } else {
                                                $scope.marker = layer;
                                                $scope.selectMarker(true);
                                            }

                                        }

                                    }
                                }

                            });

                            if(layer.options.icon.options.message === "") {
                                layer.bindPopup("write a message...");
                            } else {
                                layer.bindPopup(layer.options.icon.options.message);
                            }

                            $scope.drawnItems.addLayer(layer);
                        }
                    });
                }

                map.addLayer($scope.drawnItems);
                if(!$scope.activateHideLiner && $scope.linerLayers.features.length > 0) {
                $scope.activateHideLiner = true;
            }

            if(!$scope.activateHideMarker && $scope.markerLayers.features.length > 0) {
                $scope.activateHideMarker = true;
            }
            });    

        };

        $scope.hideMarkers = function() {
            $scope.activateHideMarker = false;
            $scope.drawnItems.eachLayer(function (layer) {
                //do whatever you want, most likely save back to db

                var customLayer = layer.toGeoJSON(); 

                if(customLayer.geometry.type === 'Point') { 
                    $scope.drawnItems._map.removeLayer(layer);
                }                         
            });                    
        };

        $scope.hideLiners = function() {
            $scope.activateHideLiner = false;
            $scope.drawnItems.eachLayer(function (layer) {
                //do whatever you want, most likely save back to db

                var customLayer = layer.toGeoJSON(); 

                if(customLayer.geometry.type === 'LineString') {                              
                    $scope.drawnItems._map.removeLayer(layer);
                }
            });                    
        };

        $scope.hideAllItems = function() {
            $scope.drawnItems.eachLayer(function (layer) {
                //do whatever you want, most likely save back to db

                if($scope.activateHideMarker) {
                    $scope.activateHideMarker = false;
                }

                if($scope.activateHideLiner) {
                    $scope.activateHideLiner = false;
                }

                $scope.drawnItems._map.removeLayer(layer);
            });                    
        };

        $scope.codeLatLng = function (latlng) {
            $scope.geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        $scope.marker.options.icon.options.address_components = results[1].address_components;                        
                        
                        console.log("$scope.marker foogle");
                        console.log($scope.marker);
                        
                        console.log("results");
                        console.log(results);
                        
                        console.log("results[1]");
                        console.log(results[1]);
                        
                        console.log("status");
                        console.log(status);

                        if($scope.liner !== null) {
                            $scope.selectLiner(false);
                            $scope.liner = null;
                        }                             
                        
                        $scope.changeMarker('address_components');
                        
                        $scope.marker.fireEvent('click');

                    } else {
                            console.log('No results found');
                    }
                } else {
                    console.log('Geocoder failed due to: ' + status);
                }
            });    
        };       
}]);

app.factory('GetContentService', function() {
    var service = {};

    service.getContent = function() {
        return {
            title:   $scope.marker.options.icon.options.title,
            content: "Some content, way to looooooooooooooooonnnnnnnnnnnnngggggggggg."
        };
    };

    return service;
});