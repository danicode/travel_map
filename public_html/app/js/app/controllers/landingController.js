'use strict';

/* Controllers */

var landingController = angular.module('landingController', []);

landingController.controller('LandingController', [
    '$scope', 
    '$http',
    '$routeParams',
    '$rootScope',
    '$location', 
    'AuthenticationService',   
    '$translate',
    '$translatePartialLoader',
    'PopupModalService', 
    'PopupModalProgressbarService',
    
    function($scope, $http, $routeParams, $rootScope, $location, AuthenticationService, $translate, $translatePartialLoader, PopupModalService, PopupModalProgressbarService) {
        
//        $rootScope.$on('$routeChangeSuccess', function () {
//            console.log($routeParams.id);
//            console.log($routeParams.type)
//        });
        
        console.log("landing controller - current path: "+$location.path());
        console.log("$routeParams");
        console.log($routeParams);
        
        if($routeParams.hasOwnProperty('token') && typeof $routeParams.token !== 'undefined') {
            var modal = {
                templateUrl : 'app/popup/reset-password-popup.html',
                windowClass : 'custom-modal',
                controller  : 'ResetPasswordPopupController',
                backdrop    : 'static',
                size        : '',
                title       : '',
                notification: '',
                token       : $routeParams.token,
                error       : false
            };

            PopupModalService.show(modal);           
        }

        $translatePartialLoader.addPart('translation');
        $translate.refresh();
        
        $rootScope.CURRENT_LANGUAGE =  $translate.use();

        $scope.credentials = { 
            email: '', 
            password: '',
            stay_signed_in: false
        };    
        
        $scope.credentials_register = {
            first_name           : '',
            last_name            : '',
            email                : '',
            password             : '',
            password_confirmation: ''
        };
        
        $scope.stay_signed_in = false;

        $scope.login = function() {
            
            var modal = {
                templateUrl : 'app/popup/progressbar-popup.html',
                windowClass : 'custom-modal',
                controller  : 'ProgressbarController',
                backdrop    : 'static',
                size        : '',
                title       : '',
                notification: '',
                error       : false
            };

            PopupModalProgressbarService.show(modal);
            
            $scope.credentials.stay_signed_in = $scope.stay_signed_in;
            AuthenticationService.login($scope.credentials).success(function() {
                PopupModalProgressbarService.close();
                $location.path('/'+$rootScope.HOME_PAGE);
            }).error(function() {
                PopupModalProgressbarService.close();
            });
        };
        
        $scope.signup = function() {
            
            var modal = {
                templateUrl : 'app/popup/progressbar-popup.html',
                windowClass : 'custom-modal',
                controller  : 'ProgressbarController',
                backdrop    : 'static',
                size        : '',
                title       : '',
                notification: '',
                error       : false
            };

            PopupModalProgressbarService.show(modal);
            
            AuthenticationService.signup($scope.credentials_register).success(function() {  
                PopupModalProgressbarService.close();
                //TODO -> must go to profile.
                $location.path('/'+$rootScope.HOME_PAGE);
            }).error(function() {
                PopupModalProgressbarService.close();
            });
        };
        
        $scope.section_hide_time = 1300;
        $scope.section_show_time = 1300;

        // Switch section
        $scope.linkToSection = function(section) {

            angular.element('li', '.header-menu').removeClass('active');
            
            if(section === $rootScope.PRIVACITY_MENU) {
                angular.element('li', '.header-menu').eq($rootScope.ELEMENT_MENU[section] + $rootScope.COUNTRY_LIST.length).addClass('active');
            } else {
                angular.element('li', '.header-menu').eq($rootScope.ELEMENT_MENU[section]).addClass('active');
            }            

            angular.element('.section:visible').fadeOut( $scope.section_hide_time, function() { 
                angular.element('a', '.header-menu').removeClass( 'active' );    
                angular.element('#'+$rootScope.CURRENT_LANGUAGE).addClass('active');
                angular.element('#'+section).fadeIn( $scope.section_show_time );
            });
        }; 
        
        /* FORGOT PASSWORD - MODAL POPUP */
        
        $scope.openPopupForgotPassword = function() {
            
            var modal = {
                templateUrl: 'app/popup/forgot-password-popup.html',
                windowClass: 'custom-modal',
                controller : 'ForgotPasswordPopupController',
                backdrop   : 'true',
                size       : ''                    
            };

            PopupModalService.show(modal);
        };   
        
        /* END FORGOT PASSWORD - MODAL POPUP */   

}]);