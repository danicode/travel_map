'use strict';

/* Controllers */

var notificationController = angular.module('notificationController', []);

notificationController.controller('NotificationController', [
    '$scope', 
    '$modalInstance',  
    'title',
    'notification',
    'error',
    function($scope, $modalInstance, title, notification, error) {
        
        $scope.title = title;
        $scope.notification = notification;
        $scope.error = error;

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };                    
    }
]);