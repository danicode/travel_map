'use strict';

/* Controllers */

var otherwiseController = angular.module('otherwiseController', []);

otherwiseController.controller('OtherwiseController', [
    '$scope', 
    'AuthenticationService',
    '$location',
    '$rootScope',
    'PopupModalService',
    '$filter',
    
    function($scope, AuthenticationService, $location, $rootScope, PopupModalService, $filter) {   
        console.log("sdadada 123456");
        
        if(AuthenticationService.isLoggedIn()) {
            $location.path('/'+$rootScope.HOME_PAGE);
        } else {
            $location.path('/'+$rootScope.LANDING_PAGE);
        }

        var modal = {
            templateUrl : 'app/popup/notication-popup.html',
            windowClass : 'custom-modal',
            controller  : 'NotificationController',
            backdrop    : 'true',
            size        : '',
            title       : $filter('translate')('lbl_notification'),
            notification: $filter('translate')('lbl_path_error'),
            error       : true
        };

        PopupModalService.show(modal);
}]);