'use strict';

/* Controllers */

var progressbarController = angular.module('progressbarController', []);

progressbarController.controller('ProgressbarController', [
    '$scope', 
    '$modalInstance',  
    'title',
    'notification',
    'error',
    function($scope, $modalInstance, title, notification, error) {
        
        $scope.title = title;
        $scope.notification = notification;
        $scope.error = error;

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };                    
    }
]);