'use strict';

/* Controllers */

var resetPasswordPopupController = angular.module('resetPasswordPopupController', []);

resetPasswordPopupController.controller("ResetPasswordPopupController", [
    '$scope', 
    '$modalInstance',  
    'PopupModalProgressbarService',
    'AuthenticationService',
    'token',
    function($scope, $modalInstance, PopupModalProgressbarService, AuthenticationService, token) {
        
        $scope.credentials_reset_password = { 
            email                : '',
            password             : '',
            password_confirmation: '',
            token                : token
        }; 

        $scope.send = function() {
            var modal = {
                templateUrl : 'app/popup/progressbar-popup.html',
                windowClass : 'custom-modal',
                controller  : 'ProgressbarController',
                backdrop    : 'static',
                size        : '',
                title       : '',
                notification: '',
                error       : false
            };

            PopupModalProgressbarService.show(modal);
            
            AuthenticationService.reset($scope.credentials_reset_password).success(function() {
                PopupModalProgressbarService.close();
            }).error(function() {
                PopupModalProgressbarService.close();
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };                    
    }
]);