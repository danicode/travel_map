'use strict';

/* Controllers */

var translateController = angular.module('translateController', []);

translateController.controller('TranslateController', [
    '$scope',
    '$rootScope',
    '$translate',
                
    function($scope, $rootScope, $translate) {
        $scope.changeLanguage = function (langKey) {
        $translate.use(langKey);
        $rootScope.CURRENT_LANGUAGE =  langKey;
    };        
       
}]);