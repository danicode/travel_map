'use strict';

/* Controllers */

var userController = angular.module('userController', []);

userController.controller('UserController', [
    '$scope', 
    '$location',
    'AuthenticationService',
    'PopupModalProgressbarService',
    '$rootScope',
    
    function($scope, $location, AuthenticationService, PopupModalProgressbarService, $rootScope) {   
        
        $scope.logout = function() {
            var modal = {
                templateUrl : 'app/popup/progressbar-popup.html',
                windowClass : 'custom-modal',
                controller  : 'ProgressbarController',
                backdrop    : 'static',
                size        : '',
                title       : '',
                notification: '',
                error       : false
            };

            PopupModalProgressbarService.show(modal);

            AuthenticationService.logout().success(function() {
                PopupModalProgressbarService.close();
                $location.path('/'+$rootScope.LANDING_PAGE);
            }).error(function() {
                PopupModalProgressbarService.close();
            });
        };
}]);


