'use strict';

/* Directives */

var directives = angular.module('ngPwCheck', []);

directives.directive("ngPwCheck", function() {
    return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.ngPwCheck;
            console.log("firstPassword");
            console.log(firstPassword);
            elem.add(firstPassword).on('keyup', function() {
                scope.$apply(function() {
                    console.info("elem.val(): "+elem.val()+" - $(firstPassword).val(): "+$(firstPassword).val());
                    if(elem.val() === '') {
                        ctrl.$setValidity('pwmatch', true);
                    } else {
                        ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                    }                    
                });
            });
        }
    };
});