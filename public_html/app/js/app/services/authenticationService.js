'use strict';

/* Services */

var authenticationService = angular.module('authenticationService', []);

authenticationService.factory('AuthenticationService', [
    '$http', 
    '$sanitize', 
    'SessionService', 
    'FlashService', 
    'CSRF_TOKEN', 
    '$translate',
    'CookieService',
    'HandlerHttpService',
    'Base64Service',
    '$rootScope',
    'PopupModalService',
    '$filter',
    
    function($http, $sanitize, SessionService, FlashService, CSRF_TOKEN, $translate, CookieService, HandlerHttpService, Base64Service, $rootScope, PopupModalService, $filter) {

        var cacheSession = function(data, stay_signed_in) { 
            stay_signed_in = stay_signed_in || false;
            
            var authdata = Base64Service.encode(data.email + ':' + data.password);
            
            data.authdata = authdata;
            
            var currentUser = parseUserData(data);             
            
            $rootScope.globals.currentUser = currentUser;

            if(stay_signed_in) {
                CookieService.set($rootScope.USER_KEY, data);
            }
            SessionService.set($rootScope.USER_KEY, data);         

            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
        };

        var uncacheSession = function() {
            console.log("execute uncacheSession");
            SessionService.unset($rootScope.USER_KEY);
            //delete $rootScope.globals;
            CookieService.remove($rootScope.USER_KEY);
            $http.defaults.headers.common.Authorization = 'Basic ';
        };

        var showMessage = function(response) {
            FlashService.show(response.flash);
        };      

        var sanitizeCredentials = function(credentials) {              
            var obj = {};
            var value;
            for (var attributeName in credentials) {
                value = credentials[attributeName];
                if(jQuery.type(value) === "string") {
                    value = $sanitize(value);
                }           
                
                obj[attributeName] = value;                     
            }

            obj.csrf_token = CSRF_TOKEN;
            obj.language   = $translate.use();

            return obj;
        };
        
        var restore = function() {
            $rootScope.globals = {
                currentUser: {
                    first_name: (SessionService.get($rootScope.USER_KEY)).first_name,
                    authdata  : (SessionService.get($rootScope.USER_KEY)).authdata
                }
            };
        };
        
        var parseUserData = function(data) {            
            var currentUser = {
                first_name: data.first_name,
                image_url : data.image_url, 
                authdata  : data.authdata
            };
            
            return currentUser;
        };

        return {
            
            login: function(credentials) {
                
                // configuration object
                var config = { 
                    method: 'POST',
                    url   : '/auth/login',
                    data  : sanitizeCredentials(credentials)
                }; 
                
                var login = $http(config);
                login.success(function(data, status, headers, config) {                   
                    cacheSession(data, credentials.stay_signed_in);
                });
                
                login.success(FlashService.clear); 

                login.error(function(data, status, headers, config) { 
                    if(!HandlerHttpService.verify(data, status)) {
                        showMessage(data);
                    }                    
                });
                
                return login;
            },
            
            logout: function() {                
                var config = { 
                    method: 'GET',
                    url   : '/auth/logout',
                    data  : sanitizeCredentials()
                };
                
                var logout = $http(config);
                logout.success(uncacheSession);      
                logout.error(function(data, status, headers, config) {
                    if(!HandlerHttpService.verify(data, status)) {
                        if(status === 400) {
                            var modal = {
                                templateUrl : 'app/popup/notication-popup.html',
                                windowClass : 'custom-modal',
                                controller  : 'NotificationController',
                                backdrop    : 'true',
                                size        : '',
                                title       : $filter('translate')('lbl_notification'),
                                notification: $filter('translate')('lbl_code_http_status_400'),
                                error       : true
                            };

                            PopupModalService.show(modal);
                        }
                    } 
                });
                return logout;
            },
            
            isLoggedIn: function() {
                return SessionService.get($rootScope.USER_KEY) !== null;
            },
            
            setCacheSession: function(data, stay_signed_in) {
                cacheSession(data, stay_signed_in);
            },
            
            restoreAccount: function() {
                restore();
            },
            
            signup: function(credentials) {
                // configuration object
                var config = { 
                    method: 'POST',
                    url   : '/auth/signup',
                    data  : sanitizeCredentials(credentials)
                }; 
                
                var signup = $http(config);
                signup.success(function(data, status, headers, config) {                   
                    cacheSession(data, false);
                });
                
                signup.success(FlashService.clear); 

                signup.error(function(data, status, headers, config) { 
                    if(!HandlerHttpService.verify(data, status)) {
                        showMessage(data);
                    }                    
                });
                
                return signup;
            },
            
            remind: function(credentials) {
                
                // configuration object
                var config = { 
                    method: 'POST',
                    url   : '/auth/remind',
                    data  : sanitizeCredentials(credentials)
                }; 
                
                var remind = $http(config);
                remind.success(function(data, status, headers, config) {                   
                    FlashService.clear;
                    showMessage(data);
                });             

                remind.error(function(data, status, headers, config) { 
                    if(!HandlerHttpService.verify(data, status)) {
                        showMessage(data);
                    }                    
                });
                
                return remind;
            },
            
            reset: function(credentials) {
                
                // configuration object
                var config = { 
                    method: 'POST',
                    url   : '/auth/reset',
                    data  : sanitizeCredentials(credentials)
                }; 
                
                var reset = $http(config);
                reset.success(function(data, status, headers, config) {                   
                    FlashService.clear;
                    showMessage(data);
                });             

                reset.error(function(data, status, headers, config) { 
                    if(!HandlerHttpService.verify(data, status)) {
                        showMessage(data);
                    }                    
                });
                
                return reset;
            }
        };
}]);