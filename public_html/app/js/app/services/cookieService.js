'use strict';

/* Services */

var cookieService = angular.module('cookieService', []);

cookieService.factory('CookieService', 
    ['$cookieStore', 
        
    function($cookieStore) {        
        
        return {
            set: function(key, value) {
                // you can retrive a user setted from another page, like login sucessful page.
                var existing_cookie_value = $cookieStore.get(key);
                value =  value || existing_cookie_value;
                $cookieStore.put(key, value);
            },
            
            get: function(key) {
                return $cookieStore.get(key);
            },

            remove: function(key) {
                $cookieStore.remove(key, $cookieStore.get(key));
            }
        };
}]);