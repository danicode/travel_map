'use strict';

/* Services */

var flashService = angular.module('flashService', []);

flashService.factory('FlashService', [
    
    '$rootScope', 
    
    function($rootScope) {
        return {
            show: function(message) {
                $rootScope.flash = message;
            },
            clear: function() {
                $rootScope.flash = '';
            }
        };
}]);