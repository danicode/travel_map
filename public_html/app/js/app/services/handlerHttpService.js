'use strict';

/* Services */

var handlerHttpService = angular.module('handlerHttpService', []);

handlerHttpService.factory('HandlerHttpService', [    
    'PopupModalService', 
    '$filter',
    '$rootScope',
    
    function(PopupModalService, $filter, $rootScope) {
        return {
            verify: function(data, status) {
                var flag = false;
                if(_($rootScope.CODE_HTTP_STATUS).contains(status)) {
                    var notification = '';
                    var error = true;

                    switch(status) {
                        //case 200: notification = $filter('translate')('lbl_code_http_status_200'); error = false; break;       
                        //case 400: notification = $filter('translate')('lbl_code_http_status_400'); break;       
                        case 401: notification = $filter('translate')('lbl_code_http_status_401'); break;                            
                        case 402: notification = $filter('translate')('lbl_code_http_status_402'); break;
                        case 403: notification = $filter('translate')('lbl_code_http_status_403'); break;
                        case 404: notification = $filter('translate')('lbl_code_http_status_404'); break;
                        case 405: notification = $filter('translate')('lbl_code_http_status_405'); break;
                        case 407: notification = $filter('translate')('lbl_code_http_status_407'); break;
                        case 415: notification = $filter('translate')('lbl_code_http_status_415'); break;
                        case 500:                            
                            notification = $filter('translate')('lbl_code_http_status_500');   
                            if(data.hasOwnProperty('flash')) {
                                if(data.flash.type === 'token_mismatch_exception') {            
                                    notification = data.flash.message;
                                }  
                            }                                                       
                            break;
                        case 501: notification = $filter('translate')('lbl_code_http_status_501'); break;
                        case 502: notification = $filter('translate')('lbl_code_http_status_502'); break;
                        case 503: notification = $filter('translate')('lbl_code_http_status_503'); break;
                    }

                    var modal = {
                        templateUrl : 'app/popup/notication-popup.html',
                        windowClass : 'custom-modal',
                        controller  : 'NotificationController',
                        backdrop    : 'true',
                        size        : '',
                        title       : $filter('translate')('lbl_notification'),
                        notification: notification,
                        error       : error
                    };

                    PopupModalService.show(modal);
                    flag = true;
                }
                
                return flag;
            }
        };
}]);