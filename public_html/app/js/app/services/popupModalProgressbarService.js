'use strict';

/* Services */

var popupModalProgressbarService = angular.module('popupModalProgressbarService', []);

popupModalProgressbarService.factory('PopupModalProgressbarService', [
    
    '$modal', 
    
    function($modal) {
        
        var _modalInstance = {};
        
        return {            
            show: function(modal) {
                
                _modalInstance = $modal.open({
                    templateUrl: modal.templateUrl,
                    windowClass: modal.windowClass,
                    controller : modal.controller,
                    backdrop   : modal.backdrop !== 'undefined' ? modal.backdrop : true,  
                    size       : modal.size,
                    resolve: {
                        title: function() {
                            return typeof modal.title !== 'undefined' ? modal.title : '';     
                        },
                        notification: function() {      
                            return typeof modal.notification !== 'undefined' ? modal.notification : '';                            
                        },
                        error: function() {
                            return typeof modal.error !== 'undefined' ? modal.error : '';     
                        }
                    }
                });
            },
            
            close: function() {
                _modalInstance.dismiss('cancel');
            }
        };
}]);


