/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(typeof tm === 'undefined' ){tm = {};}
if(typeof tm.utils === 'undefined' ){tm.utils = {};}

tm.utils.header= (function() {
    
    var pub={};
    
    // global. currently active menu item 
    var current_item = 0;
    
    var current_language;

    // few settings
    var section_hide_time = 1300;
    var section_show_time = 1300;

    // jQuery stuff
    jQuery(document).ready(function($) {	

        // Switch section
        $('a', '.header-menu .include_menu').click(function() 
        {
            
            //get the current language and save in the current_language var
            $('li', '.dropdown').each(function(index, element) {                
                if($(element).hasClass('active')) {
                    current_language = $(element);
                }
            });
            
            $('li', '.header-menu').removeClass('active');
            $(this).parent().addClass('active');  
            
            // set active to the current language, 
            // because for the sentence $('li', '.header-menu').removeClass('active');
            // is remove the selection
            current_language.addClass('active');
            
            if( ! $(this).hasClass('active') ) { 
                current_item = this;
                // close all visible divs with the class of .section
                $('.section:visible').fadeOut( section_hide_time, function() { 
                    $('a', '.header-menu').removeClass( 'active' );    
                    var new_section = $( $(current_item).attr('href') );
                    new_section.fadeIn( section_show_time );
                });
            }
            return false;
        });	    

    });

    // reaveal any variables and functions assigned to the pub object
    return pub;

}()); // the parens here cause the anonymous function to execute and return