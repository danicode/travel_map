var gulp       = require('gulp'),
    gutil      = require('gulp-util'),
    uglify     = require('gulp-uglify'),
    concat     = require('gulp-concat'),
    minifyCSS  = require('gulp-minify-css'),
    changed    = require('gulp-changed'),
    imagemin   = require('gulp-imagemin'),
    minifyHTML = require('gulp-minify-html'),
    stripDebug = require('gulp-strip-debug');  
    
    // minify new images
    gulp.task('imagemin', function() {
      var imgSrc = './app/img/**/*',
          imgDst = './dist/img';

    gulp.src(imgSrc)
        .pipe(changed(imgDst))
        .pipe(imagemin())
        .pipe(gulp.dest(imgDst));
    });
    
    // minify new or changed HTML pages
    gulp.task('htmlpage', function() {
        var opts = {comments:true,spare:true};
        
        var htmlSrc = './*.html',
            htmlDst = './dist';

    gulp.src(htmlSrc)
        .pipe(changed(htmlDst))
        .pipe(minifyHTML(opts))
        .pipe(gulp.dest(htmlDst));
    }); 
    
    gulp.task('minify-js', function() {
        gulp.src(['./app/js/**/*.js'])
        
          .pipe(concat('main-min.js'))
          .pipe(stripDebug())
          .pipe(uglify())
          .pipe(gulp.dest('./dist/js/'));
    });
    
    gulp.task('copy-lib-js', function() {
        gulp.src(['./app/bower_components/**/*.min.js', 
            './app/bower_components/**/*.min.css', 
            './app/bower_components/**/*.png', 
            './app/bower_components/**/*.eot', 
            './app/bower_components/**/*.svg', 
            './app/bower_components/**/*.ttf', 
            './app/bower_components/**/*.woff'])
          .pipe(gulp.dest('./dist/bower_components/'));
    });
    
    gulp.task('minify-css', function() {
        gulp.src(['./app/css/**/*.css'])
        .pipe(concat('main-min.css'))
        .pipe(minifyCSS({keepBreaks:true}))
        .pipe(gulp.dest('./dist/css/'));
    });

    // default gulp task
    gulp.task('default', ['imagemin', 'htmlpage', 'minify-js', 'minify-css', 'copy-lib-js'], function() {
    });